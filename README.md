# test_example
Как я вышел на проблему? Перейдя на главную страницу в браузере я отрыл режим разработчика и в html файле была такая ошибка:

<img src="images/error.jpg" style="zoom:150%;" />

Решение проблемы:

1. База данных не цеплялась к MySQL потому что в файле `config.local.php` был не правильно указан пароль от базы данных. Правильный пароль я нашёл в файле `/srv/projects/test2.host321.net/.mysql.yml`.
2. Не могу решить проблему, т.к. нет прав администратора!!!!!! Вижу что в директории `images/thumbnails` стоят права `dr--------`, это значит, что директорию, даже, нельзя перейти, у остальных директорий в `images` права `drwxrwxr-x`. В официальной [документации](https://docs.cs-cart.com/latest/install/possible_issues/problem_with_images.html) написано, что у всех файлов должно быть 777 во всей папке `images`.
   1. Если бы проблема не решилась, от смены прав, то возможно в базе данных в таблице потерялись данные
   :![](images/db0.png)
   ![](images/db1.png)
   хотя таблицы вроде бы не связаны
   :![](images/scheme.png)



Как бы я ответил на данную заявку:

Ru:
Вас приветсвует сотрудник технической поддержки Simtech Development, моё имя Святослав.
Принял ваш запрос. Вижу, что сервер пишет, что база данных не подключена к серверу, мы уже решаем вашу проблему.
En:
My name is Svyatoslav, Simtech Development technical support agent.
I have accepted your request. I see that the server writes that the database is not connected to the server, we are already solving your problem.

Как бы ответил после частичного решения проблемы:

Ru:

Спасибо за ожидание. Сделал так, что бы сайт отображался, проблему с миниатюрами не могу решить сейчас. Передам вашу заявку старшим сотрудникам с предположительным решением вашей проблемы.

Чем то могу ещё помочь?

En:

Thanks for the wait. Got the site to display, the thumbnail problem can't be resolved now. I will forward your request to senior support staff with a suggested solution to your problem.

Is there anything else I can do to help?

